extern crate parking_lot;

use std::iter::FusedIterator;
use std::ops::{Deref, DerefMut};
use std::sync::{Arc, Weak};

use parking_lot::{RwLock, RwLockReadGuard, RwLockWriteGuard};

type StrongLink<T> = Option<Arc<RwLock<NodeInner<T>>>>;
type WeakLink<T> = Option<Weak<RwLock<NodeInner<T>>>>;

#[derive(Clone, Debug)]
pub struct NodeInner<T> {
    parent: WeakLink<T>,
    first_child: StrongLink<T>,
    last_child: WeakLink<T>,
    next_sibling: StrongLink<T>,
    prev_sibling: WeakLink<T>,
    data: T,
}

#[derive(Clone, Debug)]
pub struct Node<T>(Arc<RwLock<NodeInner<T>>>);

impl<T> Node<T> {
    pub fn new(data: T) -> Node<T> {
        Node(Arc::new(RwLock::new(NodeInner {
            parent: None,
            first_child: None,
            last_child: None,
            next_sibling: None,
            prev_sibling: None,
            data: data,
        })))
    }
}

impl<T> Node<T> {
    pub fn read<'a>(&'a self) -> impl Deref<Target = T> + 'a {
        RwLockReadGuard::map(self.0.read(), |a| &a.data)
    }

    pub fn write<'a>(&'a self) -> impl DerefMut<Target = T> + 'a {
        RwLockWriteGuard::map(self.0.write(), |a| &mut a.data)
    }

    pub fn parent(&self) -> Option<Node<T>> {
        let inner = self.0.read();

        if let Some(ref parent) = inner.parent {
            parent.upgrade().map(|v| Node(v))
        } else {
            None
        }
    }

    pub fn first_child(&self) -> Option<Node<T>> {
        let inner = self.0.read();

        if let Some(ref first_child) = inner.first_child {
            Some(Node(first_child.clone()))
        } else {
            None
        }
    }

    pub fn last_child(&self) -> Option<Node<T>> {
        let inner = self.0.read();

        if let Some(ref last_child) = inner.last_child {
            last_child.upgrade().map(|v| Node(v))
        } else {
            None
        }
    }

    pub fn next_sibling(&self) -> Option<Node<T>> {
        let inner = self.0.read();

        if let Some(ref next_sibling) = inner.next_sibling {
            Some(Node(next_sibling.clone()))
        } else {
            None
        }
    }

    pub fn prev_sibling(&self) -> Option<Node<T>> {
        let inner = self.0.read();

        if let Some(ref prev_sibling) = inner.prev_sibling {
            prev_sibling.upgrade().map(|v| Node(v))
        } else {
            None
        }
    }

    pub fn children<'a>(&'a self) -> Children<'a, T> {
        Children {
            first: self.0.read().first_child.clone(),
            last: self.0.read().last_child.clone(),
            _guard: self.0.read(),
        }
    }

    pub fn ancestors<'a>(&'a self) -> Ancestors<'a, T> {
        Ancestors {
            parent: self.0.read().parent.clone(),
            _guard: self.0.read(),
        }
    }

    pub fn siblings<'a>(&'a self) -> Siblings<'a, T> {
        let parent = match self.0.read().parent.clone().and_then(|v| v.upgrade()) {
            Some(v) => v,
            None => {
                return Siblings {
                    first: None,
                    last: None,
                    _guard: self.0.read(),
                }
            }
        };

        let first = parent.read().first_child.clone();
        let last = parent.read().last_child.clone();

        Siblings {
            first: first,
            last: last,
            _guard: self.0.read(),
        }
    }

    pub fn append(&self, node: Node<T>) {
        let mut inner = self.0.write();

        node.0.write().parent = Some(Arc::downgrade(&self.0));

        if let Some(ref child) = inner.last_child.clone().and_then(|v| v.upgrade()) {
            child.write().next_sibling = Some(node.0.clone());
            node.0.write().prev_sibling = Some(Arc::downgrade(&child));
        }

        inner.last_child = Some(Arc::downgrade(&node.0));

        if let None = inner.first_child {
            inner.first_child = Some(node.0);
        }
    }

    pub fn prepend(&self, node: Node<T>) {
        let mut inner = self.0.write();

        node.0.write().parent = Some(Arc::downgrade(&self.0));

        if let Some(ref child) = inner.first_child {
            child.write().prev_sibling = Some(Arc::downgrade(&node.0));
            node.0.write().next_sibling = Some(child.clone());
        }

        inner.first_child = Some(node.0.clone());

        if let None = inner.last_child {
            inner.last_child = Some(Arc::downgrade(&node.0));
        }
    }

    pub fn ptr_eq(&self, other: &Node<T>) -> bool {
        Arc::ptr_eq(&self.0, &other.0)
    }
}

pub struct Children<'a, T: 'a> {
    first: StrongLink<T>,
    last: WeakLink<T>,
    _guard: RwLockReadGuard<'a, NodeInner<T>>,
}

impl<'a, T: 'a> Iterator for Children<'a, T> {
    type Item = Node<T>;

    fn next(&mut self) -> Option<Self::Item> {
        if let None = self.last {
            return None;
        }

        let mut ret = None;

        if let Some(ref child) = self.first {
            ret = Some(Node(child.clone()));
        }

        if let Some(ref child) = ret {
            self.first = child.0.read().next_sibling.clone();
        }

        if let Some(ref first) = self.first.clone() {
            let stop = self
                .last
                .clone()
                .and_then(|v| v.upgrade())
                .and_then(|v| v.read().next_sibling.clone())
                .filter(|v| Arc::ptr_eq(v, first))
                .and_then(|_| self.first.clone())
                .is_some();

            if stop {
                self.first = None;
            }
        };

        ret
    }
}

impl<'a, T: 'a> DoubleEndedIterator for Children<'a, T> {
    fn next_back(&mut self) -> Option<Self::Item> {
        if let None = self.first {
            return None;
        }

        let mut ret = None;

        if let Some(ref child) = self.last {
            ret = child.upgrade().map(|v| Node(v));
        }

        if let Some(ref child) = ret {
            self.last = child.0.read().prev_sibling.clone();
        }

        if let Some(ref last) = self.last.clone().and_then(|v| v.upgrade()) {
            let stop = self
                .first
                .clone()
                .and_then(|v| v.read().prev_sibling.clone())
                .and_then(|v| v.upgrade())
                .filter(|v| Arc::ptr_eq(v, last))
                .and_then(|_| self.last.clone())
                .is_some();

            if stop {
                self.last = None;
            }
        };

        ret
    }
}

impl<'a, T: 'a> FusedIterator for Children<'a, T> {}

pub struct Siblings<'a, T: 'a> {
    first: StrongLink<T>,
    last: WeakLink<T>,
    _guard: RwLockReadGuard<'a, NodeInner<T>>,
}

impl<'a, T: 'a> Iterator for Siblings<'a, T> {
    type Item = Node<T>;

    fn next(&mut self) -> Option<Self::Item> {
        if let None = self.last {
            return None;
        }

        let mut ret = None;

        if let Some(ref sibling) = self.first {
            ret = Some(Node(sibling.clone()));
        }

        if let Some(ref sibling) = ret {
            self.first = sibling.0.read().next_sibling.clone();
        }

        if let Some(ref first) = self.first.clone() {
            let stop = self
                .last
                .clone()
                .and_then(|v| v.upgrade())
                .and_then(|v| v.read().next_sibling.clone())
                .filter(|v| Arc::ptr_eq(v, first))
                .and_then(|_| self.first.clone())
                .is_some();

            if stop {
                self.first = None;
            }
        };

        ret
    }
}

impl<'a, T: 'a> DoubleEndedIterator for Siblings<'a, T> {
    fn next_back(&mut self) -> Option<Self::Item> {
        if let None = self.first {
            return None;
        }

        let mut ret = None;

        if let Some(ref siblingg) = self.last {
            ret = siblingg.upgrade().map(|v| Node(v));
        }

        if let Some(ref sibling) = ret {
            self.last = sibling.0.read().prev_sibling.clone();
        }

        if let Some(ref last) = self.last.clone().and_then(|v| v.upgrade()) {
            let stop = self
                .first
                .clone()
                .and_then(|v| v.read().prev_sibling.clone())
                .and_then(|v| v.upgrade())
                .filter(|v| Arc::ptr_eq(v, last))
                .and_then(|_| self.last.clone())
                .is_some();

            if stop {
                self.last = None;
            }
        };

        ret
    }
}

impl<'a, T: 'a> FusedIterator for Siblings<'a, T> {}

pub struct Ancestors<'a, T: 'a> {
    parent: WeakLink<T>,
    _guard: RwLockReadGuard<'a, NodeInner<T>>,
}

impl<'a, T: 'a> Iterator for Ancestors<'a, T> {
    type Item = Node<T>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut ret = None;

        if let Some(ref parent) = self.parent {
            ret = parent.upgrade().map(|v| Node(v));
        }

        if let Some(ref parent) = ret {
            self.parent = parent.0.read().parent.clone();
        }

        ret
    }
}

impl<'a, T: 'a> FusedIterator for Ancestors<'a, T> {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn read_write() {
        let a = Node::new(5);
        assert_eq!(*a.read(), 5);
        *a.write() += 5;
        assert_eq!(*a.read(), 10);
    }

    #[test]
    fn parent() {
        let root = Node::new(10);
        let child = Node::new(20);

        assert!(child.parent().is_none());

        root.append(child.clone());

        assert!(child.parent().unwrap().ptr_eq(&root));
    }

    #[test]
    fn children_simple() {
        let root = Node::new(10);
        let child_a = Node::new(20);
        let child_b = Node::new(30);

        assert!(root.first_child().is_none());
        assert!(root.last_child().is_none());

        root.append(child_a.clone());
        root.append(child_b.clone());

        assert!(root.first_child().unwrap().ptr_eq(&child_a));
        assert!(root.last_child().unwrap().ptr_eq(&child_b));
    }

    #[test]
    fn children_prepend() {
        let root = Node::new(10);
        let child_a = Node::new(20);
        let child_b = Node::new(30);

        root.prepend(child_a.clone());
        root.prepend(child_b.clone());

        assert!(root.first_child().unwrap().ptr_eq(&child_b));
        assert!(root.last_child().unwrap().ptr_eq(&child_a));
    }

    #[test]
    fn children_iter() {
        let root = Node::new(10);
        let child_a = Node::new(20);
        let child_b = Node::new(30);
        let child_c = Node::new(40);

        root.append(child_a.clone());
        root.append(child_b.clone());
        root.append(child_c.clone());

        let mut iter = root.children();

        assert!(iter.next().unwrap().ptr_eq(&child_a));
        println!("a");
        assert!(iter.next().unwrap().ptr_eq(&child_b));
        println!("b");
        assert!(iter.next().unwrap().ptr_eq(&child_c));
        println!("c");
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
    }

    #[test]
    fn children_iter_double_ended() {
        let root = Node::new(10);
        let child_a = Node::new(20);
        let child_b = Node::new(30);
        let child_c = Node::new(40);
        let child_d = Node::new(50);
        let child_e = Node::new(60);

        root.append(child_a.clone());
        root.append(child_b.clone());
        root.append(child_c.clone());
        root.append(child_d.clone());
        root.append(child_e.clone());

        let mut iter = root.children();

        assert!(iter.next().unwrap().ptr_eq(&child_a));
        println!("a");
        assert!(iter.next_back().unwrap().ptr_eq(&child_e));
        println!("e");
        assert!(iter.next().unwrap().ptr_eq(&child_b));
        println!("b");
        assert!(iter.next_back().unwrap().ptr_eq(&child_d));
        println!("d");
        assert!(iter.next().unwrap().ptr_eq(&child_c));
        println!("c");
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
        assert!(iter.next_back().is_none());
        assert!(iter.next_back().is_none());
    }

    #[test]
    fn siblings() {
        let root = Node::new(10);
        let child_a = Node::new(20);
        let child_b = Node::new(30);

        root.append(child_a.clone());
        root.append(child_b.clone());

        assert!(child_a.next_sibling().unwrap().ptr_eq(&child_b));
        assert!(child_b.prev_sibling().unwrap().ptr_eq(&child_a));
    }

    #[test]
    fn ancestors() {
        let grandparent = Node::new(10);
        let parent = Node::new(20);
        let child = Node::new(30);
        let grandchild = Node::new(40);

        child.append(grandchild.clone());
        parent.append(child.clone());
        grandparent.append(parent.clone());

        let mut iter = grandchild.ancestors();

        assert!(iter.next().unwrap().ptr_eq(&child));
        assert!(iter.next().unwrap().ptr_eq(&parent));
        assert!(iter.next().unwrap().ptr_eq(&grandparent));
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
    }

    #[test]
    fn siblings_iter() {
        let root = Node::new(10);
        let child_a = Node::new(20);
        let child_b = Node::new(30);
        let child_c = Node::new(40);

        root.append(child_a.clone());
        root.append(child_b.clone());
        root.append(child_c.clone());

        let mut iter = child_b.siblings();
        assert!(iter.next().unwrap().ptr_eq(&child_a));
        assert!(iter.next_back().unwrap().ptr_eq(&child_c));
        assert!(iter.next().unwrap().ptr_eq(&child_b));
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
        assert!(iter.next_back().is_none());
        assert!(iter.next_back().is_none());
    }
}
